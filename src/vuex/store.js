import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state={
  subMenuName:''
}

const mutations={
  renew(state,vaule){
    state.subMenuName=vaule;
  }
}

export default new Vuex.Store({
  state,mutations
});
