// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import Global from './Global'
import qs from 'qs'
Vue.config.productionTip = false

Vue.prototype.$http=axios;
Vue.prototype.$qs=qs;
Vue.prototype.$global=Global;
Vue.prototype.$params=qs.stringify(Global.params);
Vue.prototype.$defaultLanguage=localStorage.defaultLanguage!=undefined?localStorage.defaultLanguage:"zh-cn";
Vue.prototype.$pageId=getParams("pageId")!=undefined?getParams("pageId"):'13293';
Vue.prototype.$entry=getParams("entry")!=undefined?getParams("entry"):undefined;
Vue.prototype.$subMenuName=getParams("subMenuName")!=undefined?getParams("subMenuName"):'In-Room Dining';
console.log(Vue.prototype.$subMenuName);
Vue.prototype.$mainMenuName=getParams("mainMenuName")!=undefined?getParams("mainMenuName"):'In-Room Dining';
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

/*页面跳转传参*/
function getParams(key) {
  var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
  var searchUri=decodeURI(window.location.search);
  var r = searchUri.substr(1).match(reg);
  // var r = window.location.search.substr(1).match(reg);
  if (r != null) {
    return unescape(r[2]);
  }
  return null;
};
console.log("参数param1:"+getParams("pageId"));


