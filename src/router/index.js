import Vue from 'vue'
import Router from 'vue-router'
// import Order from '@/components/Order'
//  import Main from '@/components/Main'
// import MainV1 from '@/components/MainV1'
// import Detail from '@/components/Detail'
// import ShoppingCart from '@/components/ShoppingCart'

Vue.use(Router)

const router=new Router({
//  mode:'history',
  routes: [
    // {
    //   path:'/',
    //   name:'order',
    //   component:Order,
    //   children:[
    //     {path:'/',name:'mainv1',component: MainV1},
    //     {path:'main',name:'main',component: Main},
    //     {path:'detail',name:'detail',component: Detail},
    //     {path:'shoppingCart',name:'shoppingCart',component: ShoppingCart},
    //   ]
    // }
    {
      path:'/',
      name:'order',
      component:resolve=>require(['@/components/Order'],resolve),
      children:[
        {path:'/',name:'mainv1',component: resolve=>require(['@/components/MainV1'],resolve)},
        // {path:'main',name:'main',component: Main},
        {path:'detail',name:'detail',component: resolve=>require(['@/components/Detail'],resolve)},
        {path:'shoppingCart',name:'shoppingCart',component: resolve=>require(['@/components/ShoppingCart'],resolve)},
      ]
    }
  ]
})

// router.beforeEach((to, from, next)=>{
//    console.log(from);
//    if(to.name=='shoppingCart'){
//      Vue.prototype.$subMenuName=localStorage.cSubMenuName;
//    }else{
//      Vue.prototype.$subMenuName=Vue.prototype.$subMenuName_back;
//    }
//    console.log(Vue.prototype.$subMenuName);
//    next();
// })

export default router;
