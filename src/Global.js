import axios from 'axios'
import qs from 'qs'
const URL=localStorage.apiServer!=undefined?localStorage.apiServer:'https://junction.dev.havensphere.com/api/junctioncore/v1';
const params={
  hotel_id:'0086000015',
  device_id:localStorage.deviceId!=undefined?localStorage.deviceId:'84:62:23:24:25:a1',
  restaurant_id:'r001',
  device_type:'tv'
}

const httpGetRequest = (path, oparams='', success_callback, fail_callback) => {
  var qParams = qs.stringify(params);
  if (oparams != '') {
    qParams = qParams + '&' + oparams;
  }
return  new Promise(((resolve, reject) => {
    axios.get(URL + path + '?' + qParams).then(data => {
      success_callback(data);
      resolve(data);
    }).catch(error => {
      fail_callback(error);
      reject(data);
    });
  }))

}

export default {
  params,URL,httpGetRequest
}
